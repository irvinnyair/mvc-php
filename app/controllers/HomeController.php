<?php

namespace app\controllers;

use \View;
use \Controller;
use \Response;

class HomeController extends Controller {

    private $view;

    public function __construct()
    {
        
        $this->view = new Response();

    }

    public function Index() {
        //$user = User::find($id);
        //Response::twig("prueba/index");

        $pagina = "INDEX";


       echo  $this->view->twig("home/index.html", ["pagina"=>$pagina]);

      // echo $this->twig->render("prueba/index.html", ["val1"=>$hola1, "val2"=>$hola2 ,"val3"=>$hola3]);

    }

    public function About() {
        echo "About this mvc project";
    }

    public function edit($id)
    {
        $pagina = "EDIT";


       echo  $this->view->twig("home/edit.html", ["pagina"=>$pagina, "id" => $id]);
    }

}