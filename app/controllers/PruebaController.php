<?php

namespace app\controllers;

use \View;
use \Controller;
use \Response;


class PruebaController extends Controller {
 
    private $view;

    public function __construct()
    {
        
        $this->view = new Response();

    }

    public function actionIndex() {
        //$user = User::find($id);
        //Response::twig("prueba/index");

        $hola1 = "Hola1";
        $hola2 = "Hola2";
        $hola3 = "Hola3";

       echo  $this->view->twig("prueba/index.html", ["val1"=>$hola1, "val2"=>$hola2 ,"val3"=>$hola3]);

      // echo $this->twig->render("prueba/index.html", ["val1"=>$hola1, "val2"=>$hola2 ,"val3"=>$hola3]);

    }

    public function actionAbout() {
        echo "About this mvc project";
    }

}