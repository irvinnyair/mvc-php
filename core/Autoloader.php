<?php

class Autoloader {

    public function __construct() {
        $this->loadVendor();
        $this->loadAppClasses();
        $this->loadDatabase();
        //$this->loadView();
    }

    private function loadAppClasses() {
        spl_autoload_register(function($className) {
            require_once preg_replace("/\\\\/", "/", $className).".php";
        });
    }

    private function loadVendor() {
        require_once "vendor/autoload.php";
    }

    private function loadDatabase() {
        require_once CORE_PATH."Database.php";
    }

  /*   private function loadView() {
        require_once CORE_PATH."View.php";
    } */
}

new Autoloader();