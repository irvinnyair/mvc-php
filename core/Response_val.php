<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Response {

    private $loader;
    private $twig;

    public function __construct()
    {
        $this->loader = new FilesystemLoader(__DIR__ . '../../app/views');
        $this->twig = new Environment($this->loader);
    }

    public function twig($view, $vars = []) {
 
        echo $this->twig->render($view,  $vars);

        //require APP_PATH."views/".$view.".html";
    }

}