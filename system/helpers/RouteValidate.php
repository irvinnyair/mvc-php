<?php

namespace System\Helpers;

class RouteValidate
{

    public static function ValidateController($controllerName)
    {

        $res = file_exists("app/controllers/" . $controllerName . ".php") ? true : false;

        return $res;
    }

    public static function validateMethodController($controller, $method)
    {
        $res = method_exists($controller, $method) ? true : false;
            
        return $res;
    }
}
