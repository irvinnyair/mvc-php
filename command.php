<?php



if($argc != 2) {
    echo "Escriba nombre del controlador";
    exit(1);
}else
{

    $nombre = $argv[1];
    if(file_exists("app/controllers/" . $nombre . "Controller.php")){

        echo "El controlador: " . ucfirst("$nombre") . " YA existe";
    }else
    {
        //mkdir("app/controllers/". $nombre . "Controller.php");
        //mkdir("app/models/". $nombre . "Model.php /index.html.twig");
        mkdir("app/views/$nombre");
        fopen("app/views/$nombre/index.html.twig", "w+b");  


        fopen("app/controllers/". ucfirst($nombre . "Model.php"), "w+b"); 
        fopen("app/models/". ucfirst($nombre . "Model.php"), "w+b");  
    }
}


